#include "VCPluginHeaders.h"

// Interface includes
#include "IPalettePanelUtils.h"
#include "IRefPointUtils.h"
#include "IReferencePointData.h"
#include "ITransformFacade.h"
#include "IImportResourceCmdData.h"
#include "IDocument.h"
#include "IPlaceGun.h"
#include "IPanelControlData.h"
#include "ISelectableDialogSwitcher.h"
#include "IDialogController.h"
#include "ITriStateControlData.h"
#include "IMetaDataAccess.h"
#include "ILayoutUtils.h"
#include "ILayoutUIUtils.h"
#include "IDropDownListController.h"
#include "IUnitOfMeasureSettings.h"
#include "IMeasurementSystem.h"
#include "IUnitOfMeasure.h"
#include "ITextModel.h"
#include "ITextModelCmds.h"
#include "ITextControlData.h"
#include "IPanelControlData.h"
#include "IItemLockData.h"
#include "ISelectionUtils.h"
#include "IPageList.h"
#include "IHierarchy.h"
#include "ITLAdornmentUtils.h"
#include "IApplication.h"
#include "IPageItemTypeUtils.h"
#include "IScrapItem.h"
#include "ISpread.h"
#include "INewDocCmdData.h"
#include "IOpenFileCmdData.h"
#include "ImportExportUIID.h"
#include "IWindowUtils.h"
#include "ICloseWinCmdData.h"
#include "ILayoutCmdData.h"
#include "IPathUtils.h"
#include "IPathGeometry.h"
#include "ILayerList.h"
#include "INewLayerCmdData.h"
#include "ILayoutCmdData.h"
#include "IDocumentLayer.h"


#include "IK2ServiceRegistry.h"
#include "IK2ServiceProvider.h"
#include "TaggedTextFiltersID.h"
#include "AttributeBossList.h"
#include "ITextAttrUtils.h"
#include "TextID.h"
//
#include "ISelectionUtils.h"
#include "ISelectionManager.h"
#include "ILayoutSelectionSuite.h"
#include "ITextSelectionSuite.h"
#include "IConcreteSelection.h"
#include "IFrameList.h"
#include "IDropDownListController.h"
#include "IStringListControlData.h"
#include "ITriStateControlData.h"
#include "IGraphicFrameData.h"
#include "IStandOffData.h"
#include "ITextColumnSizer.h"

#include "IParcelList.h"
#include "ITextParcelListComposer.h"
#include "ITOPSplineData.h"
#include "IWaxIterator.h"
#include "IWaxLine.h"
#include "IWaxStrand.h"
#include "IGeometry.h"
#include "IGraphicMetaDataObject.h"
#include "IGraphicStateRenderObjects.h"
#include "IGraphicStateUtils.h"
#include "IGraphicStyleDescriptor.h"////////para cambiar el tamaÒo del marco del rectangulo////////////
//
#include "IConcreteSelection.h"
#include "ITextTarget.h"
#include "ITextFocus.h"
//#include "IMultiColumnTextFrame .h"
#include "IComposeScanner.h"
#include "IPlacePIData.h"
#include "IRangeData.h"
#include "IExportProvider.h"
#include "IXferBytes.h"
#include "IAttributeStrand.h"

#include "IListBoxController.h"

#include "IXMLUtils.h"
#include "IIDXMLElement.h"
#include "IXMLElementCommands.h"
//#include "SnpXMLHelper.h"

#include "ILayoutUtils.h"
// Other API includes
#include "K2SmartPtr.h"
#include "TextChar.h"
#include "CAlert.h"
#include "utils.h"
#include "IUIDData.h"

#include "SDKUtilities.h"
#include "SDKLayoutHelper.h"

#include "TransformUtils.h"	
#include "StringUtils.h"

#include "FileUtils.h"
//#include "PlatformFileSystemIterator.h"//no lo se
#include "IDocumentUtils.h"

#include "StreamUtil.h"
#include "SystemUtils.h"
#include "CmdUtils.h"
#include <stdlib.h>
#include <stdio.h>
//#include <winreg.h>
/*#include <io.h> 
#include <types.h> 
#include <stat.h> 
#include <time.h> 
*/
// Project includes
#include "ILinkObject.h"
#include "ILinkManager.h"
#include "ILink.h"
//añadidos
#include "IImportResourceCmdData.h"
#include "IDialogMgr.h"
#include "ISession.h"
#include "IWorkspace.h"
#include "IControlView.h"
#include "IBoolData.h"
#include "SysFileList.h"
#include "CAlert.h"
#include "ISaveFileDialog.h"

#include "adobe/unicode.hpp"
//#include "ODBCCnID.h"
//#include "IODBCCUtils.h"
#include "IPageItemAdornmentList.h"


#ifdef WINDOWS
#define PLATFORM_PATH_DELIMITER kTextChar_ReverseSolidus
#include "..\Interlasa_common\InterlasaUtilities.h"

#include <winsock2.h>
#include <iphlpapi.h>
#include <stdio.h>
#include <stdlib.h>

// Link with Iphlpapi.lib
#pragma comment(lib, "IPHLPAPI.lib")

#define WORKING_BUFFER_SIZE 15000
#define MAX_TRIES 3

#define MALLOC(x) HeapAlloc(GetProcessHeap(), 0, (x))
#define FREE(x) HeapFree(GetProcessHeap(), 0, (x))

#endif
#ifdef MACINTOSH
#define PLATFORM_PATH_DELIMITER kTextChar_Colon
#include "InterlasaUtilities.h"
#include <ifaddrs.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <CoreFoundation/CoreFoundation.h>
#endif

#include <sys/types.h>


#include <string.h>


#include "StringUtils.h"
#include <iostream>
#include <fstream>
#include <sstream>


//PMString DesignerPreferenciasFunctions::CrearFolderPreferencias()
//pero este esta en aqui: PMString FileTreeUtils::CrearFolderPreferencias()
PMString InterlasaUtilities::CrearFolderPreferencias(const PMString& NamePlugIn)
{  //CAlert::InformationAlert("entro CrearFolderPreferencias en Interlasa Utilities");
	PMString Archivo = "";
	PMString SeparatorPath=PMString("",PMString::kDontTranslateDuringCall);
	 #if defined(MACINTOSH)
		SDKUtilities::GetApplicationFolder(Archivo);
		SeparatorPath=":";
	#elif defined(WINDOWS)
	Archivo="C:\\Windows";	//ORIGINAL
   	SeparatorPath="\\";
	#endif
	
	Archivo.Append(SeparatorPath + "asalretni" + SeparatorPath);
	IDFile fileFolder;
		
		FileUtils::PMStringToIDFile(Archivo,fileFolder);
		
		if(FileUtils::CreateFolderIfNeeded(fileFolder)<0)
		{//Fue creado  
			Archivo.Append("Software" + SeparatorPath);
			FileUtils::PMStringToIDFile(Archivo,fileFolder);
			if(FileUtils::CreateFolderIfNeeded(fileFolder)<0)
			{//fue creado
				Archivo.Append(NamePlugIn + "" + SeparatorPath);
				FileUtils::PMStringToIDFile(Archivo,fileFolder);
				if(FileUtils::CreateFolderIfNeeded(fileFolder)<0)
				{//fue creado
					Archivo.Append("Preferencias" + SeparatorPath);
					FileUtils::PMStringToIDFile(Archivo,fileFolder);
					if(FileUtils::CreateFolderIfNeeded(fileFolder)<0)
					{//fue Creado
						//Archivo.Append("C2PPref.pfa");
					}
					else
					{
						//Archivo.Append("C2PPref.pfa");
					}
				}
				else
				{
					Archivo.Append("Preferencias" + SeparatorPath);
					FileUtils::PMStringToIDFile(Archivo,fileFolder);
					if(FileUtils::CreateFolderIfNeeded(fileFolder)<0)
					{//fue Creado
						//Archivo.Append("C2PPref.pfa");
					}
					else
					{
						//Archivo.Append("C2PPref.pfa");
					}
				}

			}
			else
			{
				Archivo.Append(NamePlugIn + "" + SeparatorPath);
				FileUtils::PMStringToIDFile(Archivo,fileFolder);
				if(FileUtils::CreateFolderIfNeeded(fileFolder)<0)
				{//fue creado
					Archivo.Append("Preferencias" + SeparatorPath);
					FileUtils::PMStringToIDFile(Archivo,fileFolder);
					if(FileUtils::CreateFolderIfNeeded(fileFolder)<0)
					{//fue Creado
						//Archivo.Append("C2PPref.pfa");
					}
					else
					{
						//Archivo.Append("C2PPref.pfa");
					}
				}
				else
				{
					Archivo.Append("Preferencias" + SeparatorPath);
					FileUtils::PMStringToIDFile(Archivo,fileFolder);
					if(FileUtils::CreateFolderIfNeeded(fileFolder)<0)
					{//fue Creado
						//Archivo.Append("C2PPref.pfa");
					}
					else
					{
						//Archivo.Append("C2PPref.pfa");
					}
				}
			}
		}
		else//ya habia sido creado
		{
			Archivo.Append("Software" + SeparatorPath);
		//	Archivo.Append("Plantillero_Generatos:");
		//	Archivo.Append("Preferencias:");
			FileUtils::PMStringToIDFile(Archivo,fileFolder);
			if(FileUtils::CreateFolderIfNeeded(fileFolder)<0)
			{//fue creado
				Archivo.Append(NamePlugIn + "" + SeparatorPath);
				FileUtils::PMStringToIDFile(Archivo,fileFolder);
				if(FileUtils::CreateFolderIfNeeded(fileFolder)<0)
				{//fue creado
					Archivo.Append("Preferencias" + SeparatorPath);
					FileUtils::PMStringToIDFile(Archivo,fileFolder);
					if(FileUtils::CreateFolderIfNeeded(fileFolder)<0)
					{//fue Creado
						//Archivo.Append("C2PPref.pfa");
					}
					else
					{
						//Archivo.Append("C2PPref.pfa");
					}
				}
				else
				{
					Archivo.Append("Preferencias" + SeparatorPath);
					FileUtils::PMStringToIDFile(Archivo,fileFolder);
					if(FileUtils::CreateFolderIfNeeded(fileFolder)<0)
					{//fue Creado
						//Archivo.Append("C2PPref.pfa");
					}
					else
					{
						//Archivo.Append("C2PPref.pfa");
					}
				}
			}
			else
			{
				Archivo.Append(NamePlugIn + "" + SeparatorPath);
				FileUtils::PMStringToIDFile(Archivo,fileFolder);
				if(FileUtils::CreateFolderIfNeeded(fileFolder)<0)
				{//fue creado
					Archivo.Append("Preferencias" + SeparatorPath);
					FileUtils::PMStringToIDFile(Archivo,fileFolder);
					if(FileUtils::CreateFolderIfNeeded(fileFolder)<0)
					{//fue Creado
						//Archivo.Append("C2PPref.pfa");
					}
					else
					{//fue Creado
						//Archivo.Append("C2PPref.pfa");
					}
				}
				else
				{
					Archivo.Append("Preferencias" + SeparatorPath);
					FileUtils::PMStringToIDFile(Archivo,fileFolder);
					if(FileUtils::CreateFolderIfNeeded(fileFolder)<0)
					{//fue Creado
						//Archivo.Append("C2PPref.pfa");
					}
					else
					{//fue Creado
						//Archivo.Append("C2PPref.pfa");
					}
				}
			}
		}
	return(Archivo);
}











bool16 InterlasaUtilities::GetTextOfDialog(IControlView *DlgPrefView,	const WidgetID& Combowidget, PMString& itemToShow)
{
	bool16 retval=kTrue;
	do
	{
		if(DlgPrefView==nil)
		{
			retval=kFalse;
			break;
		}
		
		InterfacePtr<IDialogController> DlgPrefController(DlgPrefView,IID_IDIALOGCONTROLLER);
		if(DlgPrefController==nil)
		{
			retval=kFalse;
			break;
		}
	
		InterfacePtr<IPanelControlData> panel(DlgPrefView, UseDefaultIID());
		if (panel == nil)
		{
			ASSERT_FAIL("PlanGenUtils::LlenarPreferenciasDeDialogoPref panel");
			break;
		}


		
		IControlView *ControlView = panel->FindWidget(Combowidget);
		if(ControlView==nil)
		{
			ASSERT_FAIL("ControlView");
			break;
		}
		
		//obtiene una interface tel tipo ITextControlData para obtener el texto de esta caja o Widget
		InterfacePtr<ITextControlData>	TextoDeDireccion( ControlView, UseDefaultIID());
		if(TextoDeDireccion==nil)
		{
			//T>CAlert::InformationAlert("No se encontro el texto de direccion");
			break;
		}
		
		itemToShow = TextoDeDireccion->GetString();
		retval=kTrue;
		
	}while(false);
	return(retval);
}







/* TruncatePath
*/
PMString InterlasaUtilities::TruncatePath(const PMString& fromthis)
{
	if(fromthis.IsEmpty() == kTrue) return PMString("Empty!");

	PMString retval = fromthis;
	int32 lastpos = (-1);
	for (int32 i = 0 ; i < fromthis.CharCount();i++)
	{
		bool16 predicate = (fromthis[i] == PLATFORM_PATH_DELIMITER);
		if (predicate)
		{
			lastpos = i;
		}
	}

	if(lastpos >= 0)
	{
		// Suppose we have ../X/Y.gif
		// Then, lastpos would be 4, and we'd want 5 chars from 5 onwards
		int32 countChars = fromthis.CharCount() - (lastpos+1);
		int32 startIndex = lastpos+1;
		int32 endIndex = (startIndex + countChars);
		if((endIndex > startIndex) && (endIndex <= fromthis.CharCount()))
		{
			K2::scoped_ptr<PMString> ptrRightStr(fromthis.Substring(startIndex, countChars));
			if(ptrRightStr)
			{
				retval = *ptrRightStr;
			}
		}
	} 
	return retval;
}



bool16 InterlasaUtilities::validPath(const PMString& p)
{
	const PMString thisDir(".");
	const PMString parentDir("..");
	return p != thisDir && p != parentDir;
}


/* TruncatePath
*/
PMString InterlasaUtilities::TruncateExtencion(const PMString& fromthis)
{
	if(fromthis.IsEmpty() == kTrue) return PMString("Empty!");

	PMString retval = fromthis;
	int32 lastpos = (-1);
	lastpos=fromthis.IndexOfString("."); 

	if(lastpos >= 0)
	{
		// Suppose we have ../X/Y.gif
		// Then, lastpos would be 4, and we'd want 5 chars from 5 onwards
		int32 countChars = lastpos;
		int32 startIndex = 0;
		int32 endIndex = (startIndex + countChars);
		if((endIndex > startIndex) && (endIndex <= fromthis.CharCount()))
		{
			K2::scoped_ptr<PMString> ptrRightStr(fromthis.Substring(startIndex, countChars));
			if(ptrRightStr)
			{
				retval = *ptrRightStr;
			}
		}
	} 
	return retval;
}






/*bool16 InterlasaUtilities::CreateTags(const UIDRef& documentUIDRef, const K2Vector<PMString>& tagNames)
{
	bool16 retval=kFalse;
	do
	{
		// +precondition
		InterfacePtr<IDocument> document(documentUIDRef, UseDefaultIID());
		ASSERT(document);
		if(!document) {
			break;
		}
		// -precondition
		SnpXMLHelper xmlHelper;
		K2Vector<PMString>::const_iterator iter;
		
		for(iter = tagNames.begin(); iter != tagNames.end(); ++iter) {
			PMString currTagName = *iter;
			ASSERT(currTagName.CharCount()>0);
			UIDRef acquiredTagUIDRef = xmlHelper.AcquireTag(documentUIDRef,currTagName);
			bool16 validTag = acquiredTagUIDRef.GetUID() != kInvalidUID; 
			ASSERT(validTag);
			if(!validTag) {
				break;
			}
		}
	}while(false);
	return(retval);
}*/


/*bool16 InterlasaUtilities::TagGraphic(const PMString& TagName, const UIDRef& GraphicFrameUIDRef)
{
	bool16 retval=kFalse;
	do
	{
		
		
		
		IDataBase* database=GraphicFrameUIDRef.GetDataBase();
		UIDRef documentUIDRef(database, database->GetRootUID());
		
		
		//+precondiciones
		InterfacePtr<IGraphicFrameData> graphicFrameData(GraphicFrameUIDRef, UseDefaultIID());
		ASSERT(graphicFrameData);
		if(!graphicFrameData)
		{
			//T>CAlert::InformationAlert("TagGraphic graphicFrameData");
			break;	
		}
		
		
		InterfacePtr<IIDXMLElement> rootXMLElement(Utils<IXMLUtils>()->QueryRootElement(database));
		ASSERT(rootXMLElement);
		if(!rootXMLElement)
		{
			//T>CAlert::InformationAlert("TagGraphic rootXMLElement");
			break;
		}
		
		
		
		XMLReference rootXMLReference = rootXMLElement->GetXMLReference();
		
		
		int32 indexInParent = rootXMLElement->GetChildCount();
		if(indexInParent<0)
		{
			
			indexInParent=0;
		}
		
		
		bool16 isTaggable = Utils<IXMLUtils>()->IsTaggablePageItem(graphicFrameData);
		ASSERT(isTaggable);
		if(!isTaggable)
		{
			//T>CAlert::InformationAlert("TagGraphic isTaggable");
			break;
		}
		
		SnpXMLHelper xmlHelper;
		
		UIDRef acquiredTagUIDRef = xmlHelper.AcquireTag(documentUIDRef,TagName);
		
		
		Utils<IXMLElementCommands>()->CreateElement( acquiredTagUIDRef.GetUID(),
											GraphicFrameUIDRef.GetUID(),
											rootXMLReference,
											indexInParent);
											
		
	}while(true);
	return(retval);
}*/



bool16 InterlasaUtilities::RemoveXMPVar(PMString Variable, IDocument* doc)
{
	bool16 retval=kFalse;
	do 
	{
		
		if (doc == nil) 
		{
			ASSERT_FAIL("You do not have a document open. This snippet requires a front document.");
			break;
		}
		//IDataBase* db = ::GetDataBase(doc);
		
		//db->BeginTransaction();
		
		
		InterfacePtr<IMetaDataAccess> metaDataAccess(doc, UseDefaultIID());
		if (metaDataAccess == nil)
		{
			CAlert::ErrorAlert("metaDataAccess is nil!");
			ASSERT_FAIL("metaDataAccess is nil!");
			break;
		}

		int32 nodesVisited = 0;
		//MetaDataFeatures metaDataFeatures;
		if(metaDataAccess->PropertyExists("http://ns.adobe.com/xap/1.0/",Variable))
			metaDataAccess->Remove("http://ns.adobe.com/xap/1.0/", Variable);
		//db->EndTransaction();
		retval=kTrue;
	} while(false);
	return(retval);
}


bool16 InterlasaUtilities::SaveXMPVar(PMString Variable,PMString value,  IDocument* doc)
{
	bool16 retval=kFalse;
	do 
	{
		
		if (doc == nil) 
		{
			ASSERT_FAIL("You do not have a document open. This snippet requires a front document.");
			
			break;
		}
		
		//IDataBase* db = ::GetDataBase(doc);
		
		//db->BeginTransaction();
		
		
		InterfacePtr<IMetaDataAccess> metaDataAccess(doc, UseDefaultIID());
		if (metaDataAccess == nil)
		{
			CAlert::ErrorAlert("metaDataAccess is nil!");
			ASSERT_FAIL("metaDataAccess is nil!");
			break;
		}

		int32 nodesVisited = 0;
		//MetaDataFeatures metaDataFeatures;

		metaDataAccess->Set("http://ns.adobe.com/xap/1.0/", Variable, value/*, metaDataFeatures*/);
		//db->EndTransaction();
		retval=kTrue;
	} while(false);
	return(retval);
}


PMString InterlasaUtilities::GetXMPVar(PMString Variable, IDocument* doc)
{
	PMString value;
	do 
	{
		
		if (doc == nil) 
		{
			ASSERT_FAIL("You do not have a document open.  This snippet requires a front document.");
			break;
		}
		
		IDataBase* db = ::GetDataBase(doc);
		
		db->BeginTransaction();
		
		
		InterfacePtr<IMetaDataAccess> metaDataAccess(doc, UseDefaultIID());
		if (metaDataAccess == nil)
		{
			ASSERT_FAIL("metaDataAccess is nil!");
			db->EndTransaction();
			break;
		}

		int32 nodesVisited = 0;
		//MetaDataFeatures metaDataFeatures;

		if(metaDataAccess->PropertyExists("http://ns.adobe.com/xap/1.0/",Variable))
		if(!metaDataAccess->Get("http://ns.adobe.com/xap/1.0/", Variable, value/*, metaDataFeatures*/))
		{
			db->EndTransaction();
			break;
		}
		db->EndTransaction();
	} while(false);
	return(value);
}


bool16 InterlasaUtilities::AppendXMPArrayItem( IDocument* doc,const PMString& structName,const PMString& Elemento)
{
	bool16 value=kFalse;
	do 
	{
		
		if (doc == nil) 
		{
			ASSERT_FAIL("You do not have a document open.  This snippet requires a front document.");
			break;
		}
		
		//IDataBase* db = ::GetDataBase(doc);
		
		//db->BeginTransaction();
		
		
		InterfacePtr<IMetaDataAccess> metaDataAccess(doc, UseDefaultIID());
		if (metaDataAccess == nil)
		{
			ASSERT_FAIL("metaDataAccess is nil!");
			break;
		}

		metaDataAccess->AppendArrayItem("http://ns.adobe.com/xap/1.0/",structName,Elemento, metadata_alt);	
		
		value=kTrue;
		//db->EndTransaction();
	} while(false);
	return(value);
}


bool16 InterlasaUtilities::SetXMPArrayItem( IDocument* doc,const PMString& structName,const PMString& Elemento, const int32& numElemento)
{
	bool16 value=kFalse;
	do 
	{
		
		if (doc == nil) 
		{
			ASSERT_FAIL("You do not have a document open.  This snippet requires a front document.");
			break;
		}
		
		//IDataBase* db = ::GetDataBase(doc);
		
		//db->BeginTransaction();
		
		
		InterfacePtr<IMetaDataAccess> metaDataAccess(doc, UseDefaultIID());
		if (metaDataAccess == nil)
		{
			ASSERT_FAIL("metaDataAccess is nil!");
			break;
		}

		metaDataAccess->SetArrayItem("http://ns.adobe.com/xap/1.0/",structName,numElemento,Elemento);	
		
		value=kTrue;
		//db->EndTransaction();
	} while(false);
	return(value);
}

bool16 InterlasaUtilities::GetXMPArrayItem(IDocument* doc,const PMString& structName,PMString& Elemento, const int32& numElemento)
{
	bool16 value=kFalse;
	do 
	{
		
		if (doc == nil) 
		{
			ASSERT_FAIL("You do not have a document open.  This snippet requires a front document.");
			break;
		}
		
		//IDataBase* db = ::GetDataBase(doc);
		
		//db->BeginTransaction();
		
		
		InterfacePtr<IMetaDataAccess> metaDataAccess(doc, UseDefaultIID());
		if (metaDataAccess == nil)
		{
			ASSERT_FAIL("metaDataAccess is nil!");
			break;
		}

		value = metaDataAccess->GetArrayItem("http://ns.adobe.com/xap/1.0/",structName,numElemento,Elemento);	
		
		
		//db->EndTransaction();
	} while(false);
	return(value);
}


bool16 InterlasaUtilities::DeleteXMPArrayItem(IDocument* doc,const PMString& structName, const int32& numElemento)
{
	bool16 value=kFalse;
	do 
	{
		
		if (doc == nil) 
		{
			ASSERT_FAIL("You do not have a document open.  This snippet requires a front document.");
			break;
		}
		
		//IDataBase* db = ::GetDataBase(doc);
		
		//db->BeginTransaction();
		
		
		InterfacePtr<IMetaDataAccess> metaDataAccess(doc, UseDefaultIID());
		if (metaDataAccess == nil)
		{
			ASSERT_FAIL("metaDataAccess is nil!");
			break;
		}

		 metaDataAccess->DeleteArrayItem("http://ns.adobe.com/xap/1.0/",structName,numElemento);	
		
		value= kTrue;
		//db->EndTransaction();
	} while(false);
	return(value);
}


int32 InterlasaUtilities::CountXMPArrayItem(IDocument* doc,const PMString& structName)
{
	int32 value=0;
	do 
	{
		
		if (doc == nil) 
		{
			ASSERT_FAIL("You do not have a document open.  This snippet requires a front document.");
			break;
		}
		
		//IDataBase* db = ::GetDataBase(doc);
		
		//db->BeginTransaction();
		
		
		InterfacePtr<IMetaDataAccess> metaDataAccess(doc, UseDefaultIID());
		if (metaDataAccess == nil)
		{
			ASSERT_FAIL("metaDataAccess is nil!");
			break;
		}
		
		bool stillin=kTrue;
		int numElem=1;
		do
		{
			PMString elemento;
			if (metaDataAccess->GetArrayItem("http://ns.adobe.com/xap/1.0/",structName,numElem,elemento) == kTrue)	
			{
				value++;
				numElem++;
			}
			else
			{
				stillin=kFalse;
			}
		}while(stillin);
		
		//db->EndTransaction();
	} while(false);
	return(value);
}

bool16 InterlasaUtilities::CreateAndProcessOpenDocCmd(const IDFile& theFile)
{
	bool16 retval=kFalse;
	do
	{
		InterfacePtr<IApplication> app(GetExecutionContextSession()->QueryApplication());
		PMString Aplicacion=app->GetApplicationName();
				
		
				
		
			
		if(Aplicacion.Contains("InDesign"))
		{
			// Create an OpenFileWithWindowCmd:
			InterfacePtr<ICommand>	openFileCmd(CmdUtils::CreateCommand(kOpenFileWithWindowCmdBoss));
			if(openFileCmd==nil)
				break;
			// Get an IOpenFileCmdData Interface for the OpenFileWithWindowCmd:
			InterfacePtr<IOpenFileCmdData> openFileData(openFileCmd, IID_IOPENFILECMDDATA);
			if(openFileData==nil)
				break;
			// Set the IOpenFileCmdData Interface's data:
			openFileData->Set(theFile,kSuppressUI);
			// Process the OpenFileWithWindowCmd:
			if (CmdUtils::ProcessCommand(openFileCmd) != kSuccess)
			{
				
				retval=kFalse;
				break;
			}
			retval=kTrue;
		}
		
	}while(false);
	return(retval);
}



int32 InterlasaUtilities::FindItem_ToNameAndValue_OnXMPArrayItem(IDocument* doc,const PMString& structName,const PMString Name,const PMString& Value)
{
	int32 value=0;
	do 
	{
		
		if (doc == nil) 
		{
			ASSERT_FAIL("You do not have a document open.  This snippet requires a front document.");
			break;
		}
		
		//IDataBase* db = ::GetDataBase(doc);
		
		//db->BeginTransaction();
		
		
		InterfacePtr<IMetaDataAccess> metaDataAccess(doc, UseDefaultIID());
		if (metaDataAccess == nil)
		{
			ASSERT_FAIL("metaDataAccess is nil!");
			break;
		}
		
		bool stillin=kTrue;
		int numElem=1;
		do
		{
			PMString elemento;
			if (metaDataAccess->GetArrayItem("http://ns.adobe.com/xap/1.0/",structName,numElem,elemento) == kTrue)	
			{
				value++;
				numElem++;

				CharCounter index=elemento.IndexOfString(Name,0);
				if(index!=-1)
				{
					
					CharCounter indexOfDel=elemento.IndexOfString("^",index);
					
					PMString Contenido=elemento.Substring( index , (indexOfDel-index) )->GetUTF8String().c_str();
					if(Contenido.Contains(Value))
					{
						stillin=kFalse;
					}
				}
			}
			else
			{
			   	value=0;
				stillin=kFalse;
			}
		}while(stillin);
		
		//db->EndTransaction();
	} while(false);
	return(value);
}



int32 InterlasaUtilities::FindItem_ToValue_OnXMPArrayItem(IDocument* doc,const PMString& structName,const PMString& Value,PMString& NameElement)
{
	int32 value=0;
	do 
	{
		
		if (doc == nil) 
		{
			ASSERT_FAIL("You do not have a document open.  This snippet requires a front document.");
			break;
		}
		
		//IDataBase* db = ::GetDataBase(doc);
		
		//db->BeginTransaction();
		
		
		InterfacePtr<IMetaDataAccess> metaDataAccess(doc, UseDefaultIID());
		if (metaDataAccess == nil)
		{
			ASSERT_FAIL("metaDataAccess is nil!");
			break;
		}
		
		bool stillin=kTrue;
		int numElem=1;
		do
		{
			PMString elemento;
			//CAlert::InformationAlert("structname="+structName);
			//CAlert::InformationAlert("Value="+Value);
			if (metaDataAccess->GetArrayItem("http://ns.adobe.com/xap/1.0/",structName,numElem,elemento) == kTrue)	
			{
				value++;
				numElem++;
				
				//CAlert::InformationAlert("elemento="+elemento);
				
				PMString *TokensOnElemnt = elemento.GetItem("^",1);
				PMString strSubString = elemento;
				int32 nCountTags = 0;
				bool16 sale = kFalse;
				while(TokensOnElemnt &&  sale==kFalse)
				{
					nCountTags++;
					PMString strItem = *TokensOnElemnt;
					
					if(strItem.Contains(Value))
					{
						//CAlert::InformationAlert("Token="+strItem);
						PMString copia=strItem;
						PMString *NameToken=copia.GetItem(":",1);
						
						//CAlert::InformationAlert("hay mama");
						if(NameToken)
						{
							NameElement=*NameToken;
						}
						
						//CAlert::InformationAlert("hay papa");
						sale=kTrue;
						stillin=kFalse;
					}
					
					if(strSubString.CharCount()<=strItem.CharCount()+1)
					{
						sale=kTrue;
					}
					else
					{
						PMString *pSubstring = strSubString.Substring(strItem.CharCount()+1);
				
						
						strSubString = *pSubstring;
						
						TokensOnElemnt = strSubString.GetItem("^",1);
						
					}
					
				}
				
				if(sale==kFalse)
				{
					value=0;
				}
				
			}
			else
			{
			   	value=0;
				stillin=kFalse;
			}
		}while(stillin);
		
		//db->EndTransaction();
	} while(false);
	return(value);
}


void InterlasaUtilities::CreateAndProcessCloseWinCmd(IWindow *win)
{
	// Create a CloseWinCmd:
	InterfacePtr<ICommand> closeCmd(CmdUtils::CreateCommand(kCloseWinCmdBoss));
	// Get an ICloseWinCmdData Interface for the CloseWinCmd:
	InterfacePtr<ICloseWinCmdData> closeData(closeCmd, IID_ICLOSEWINCMDDATA);
	// Set the ICloseWinCmdData Interface's data:
	closeData->Set(win);
	// Process the CloseWinCmd:
	if (CmdUtils::ProcessCommand(closeCmd) != kSuccess)
	{
		ErrorUtils::PMSetGlobalErrorCode(kFailure);
	}
}


ErrorCode InterlasaUtilities::CenterContentInFrame(const UIDRef& contentUIDRef)
{
	ErrorCode error=kFailure;
	do
	{
		// Create a FitContentToFrameCmd:
		InterfacePtr<ICommand>	fitCmd(CmdUtils::CreateCommand(kCenterContentInFrameCmdBoss));
		// Set the FitContentToFrameCmd's ItemList:
		fitCmd->SetItemList(UIDList(contentUIDRef));
		// Process the FitContentToFrameCmd:
		error=CmdUtils::ProcessCommand(fitCmd);
			
	}while(false);
	return error;	
}



PMString InterlasaUtilities::ArmaFecha(PMString fecha,const PMString& Idioma, PMString& ano,
	PMString& mes,
	PMString& Dia)
{
	
	
	PMString *TokensOnElemnt = fecha.GetItem("/",1);
	PMString strSubString = fecha;
	int32 nCountTags = 0;
	bool16 sale = kFalse;
	while(TokensOnElemnt && sale==kFalse)
	{
		nCountTags++;
		
		switch(nCountTags)
		{
			case 1:
				ano = *TokensOnElemnt;
				break;
			case 2:
				mes = *TokensOnElemnt;
				break;
			case 3:
				Dia = *TokensOnElemnt;
				break;
		}
		
		PMString strItem = *TokensOnElemnt;
		
		if(strSubString.CharCount()<=strItem.CharCount()+1)
		{
			sale=kTrue;
		}
		else
		{
			PMString *pSubstring = strSubString.Substring(strItem.CharCount()+1);
			strSubString = *pSubstring;
			TokensOnElemnt = strSubString.GetItem("/",1);
		}
	}
	
	Dia=strSubString;
	
    Dia=InterlasaUtilities::RemoveCharactersReturn(Dia);

	
/*	
	//CAlert::InformationAlert(fecha);
	PMString* mes=fecha.Substring(0,fecha.IndexOfWChar(47));
	fecha.Remove(0,fecha.IndexOfWChar(47)+1);
	
	PMString* Dia=fecha.Substring(0,fecha.IndexOfWChar(47));
	fecha.Remove(0,fecha.IndexOfWChar(47)+1);

	//CAlert::InformationAlert("Dia="+Dia->GrabCString());
	
	
	
	//CAlert::InformationAlert("mes="+mes->GrabCString());

	PMString* ano=fecha.Substring(0,fecha.NumUTF16TextChars());
	
	//CAlert::InformationAlert("año="+ano->GrabCString());
*/
//	time_t tiempo;
//	char cad[80];
	//struct tm *tmPtr;
	
	//tiempo = time(NULL);
	//setlocale(LC_TIME,"es_ES");

	//tmPtr = localtime(&tiempo);

	//	PMString me_s;
		
		
	
	  fecha=DayWek(Dia.GetAsNumber(), mes.GetAsNumber(), ano.GetAsNumber(),Idioma);

		
		if(Idioma=="English" || Idioma=="Ingles")
		{
			fecha.Append(", ");
			switch(mes.GetAsNumber())
			{
				case 1:
					fecha.Append("Juanary");
					break;
				case 2:
					fecha.Append("Frebruary");
					break;
				case 3:
					fecha.Append("March");
					break;
					
				case 4:
					fecha.Append("April");
					break;
				case 5:
					fecha.Append("May");
					break;
					
				case 6:
					fecha.Append("June");
					break;
				case  7:
					fecha.Append("Jule");
					break;
					
				case 8:
					fecha.Append("August");
					break;
				case 9:
					fecha.Append("September");
					break;
				case 10:
					fecha.Append("October");
					break;
				case 11:
					fecha.Append("November");
					break;
				case 12:
					fecha.Append("December");
					break;
			}
			
			fecha.Append(Dia.GetUTF8String().c_str());
			fecha.Append(", ");
		 	fecha.Append(ano.GetUTF8String().c_str());
		}
		
		if(Idioma=="Spanish" || Idioma=="Español")
		{
			fecha.Append(" ");

			fecha.Append(Dia.GetUTF8String().c_str());
			fecha.Append(" de ");
			
			switch(mes.GetAsNumber())
			{
				case 1:
					fecha.Append("Enero");
					break;
				case 2:
					fecha.Append("Febrero");
					break;
				case 3:
					fecha.Append("Marzo");
					break;
					
				case 4:
					fecha.Append("Abril");
					break;
				case 5:
					fecha.Append("Mayo");
					break;
					
				case 6:
					fecha.Append("Junio");
					break;
				case  7:
					fecha.Append("Julio");
					break;
					
				case 8:
					fecha.Append("Agosto");
					break;
				case 9:
					fecha.Append("Septiembre");
					break;
				case 10:
					fecha.Append("Octubre");
					break;
				case 11:
					fecha.Append("Noviembre");
					break;
				case 12:
					fecha.Append("Diciembre");
					break;
			}
		 	
		 	fecha.Append(" de ");
		 	fecha.Append(ano.GetUTF8String().c_str());
		}
	  
	 
	  /*tmPtr->tm_mon=mes->GetAsNumber();

	  tmPtr->tm_mday= Dia->GetAsNumber();

	  tmPtr->tm_year=ano->GetAsNumber();
	  
	  me_s.AppendNumber( tmPtr->tm_mon);
	 CAlert::InformationAlert(me_s);

	  fecha.Append(" ");
	  mktime(tmPtr);
	  strftime( cad, 80,"%d de %B de 20%y",  Dia->GetAsNumber(),Dia->GetAsNumber(),ano->GetAsNumber());*/

	 // fecha.Append(cad);
	  return(fecha);
}



PMString InterlasaUtilities::DayWek(int dd,int mm,int yyyy,const PMString& Idioma)
{
    //int dd, mm, yyyy;
    int days;
    char daysInWord[11];
    
    /* Read a date and validate the date */


       /* do{
        printf("Enter a date(dd/mm/yyyy) :");
        scanf("%d / %d / %d", &dd, &mm ,&yyyy);
        fflush(stdin);
        }
        */
        do
        {
        	
        }
        while(validateDate(dd, mm, yyyy));
        
        /* Calculate the day for Dec 31 of the previous year */
        days = calcDay_Dec31(yyyy);
        /* Calculate the day for the given date */
        days = (dayInYear(dd, mm) + days) % 7;
        /* Add one day if the year is leap year and desired date is after February */
        if ((!(yyyy % 4) && (yyyy % 100) || !(yyyy % 400)) && mm > 2)
        days++;
        nameInStr(daysInWord, days,Idioma);
        /* Print the day of the desired date */
        //printf("The day for date %d/%d/%d is %s\n\n", dd, mm, yyyy, daysInWord);
		return(daysInWord);
    } /* main */


int InterlasaUtilities::validateDate(int dd, int mm, int yyyy)
{
	int i = 0, j = 0;
    int a[7] = {1, 3, 5, 7, 8, 10, 12};
    int b[4] = {4, 6, 9, 11};
    int error = 0;
    if (mm < 1 || mm > 12)
    error = 1;
    if (mm == 2)
    {
		if (!(yyyy % 4) && (yyyy % 100) || !(yyyy % 400))
        {
			if (dd < 1 || dd > 29)
				error = 1;
                
        }
        else if (dd < 1 || dd >28)
            error = 1;
    }
                
    for (i=0;i<6;i+=1)
    {
      if (mm == a[i])
      {
		if (dd < 1 || dd > 31)
			error = 1;
      }
    }
	for (j=0;j<4;j+=1)
    {
		if (mm == b[j])
        {
			if (dd < 1 || dd > 30)
				error = 1;
        }
    }
    if (error == 1)
		printError();
    return error;
}

void InterlasaUtilities::printError()
{
	CAlert::ErrorAlert("Invalid Input!\n\n");
}

/**
*/
int InterlasaUtilities::calcDay_Dec31(int yyyy)
{
	int dayCode = 0;
    dayCode = ((yyyy-1)*365 + (yyyy-1)/4 - (yyyy-1)/100 + (yyyy-1)/400) % 7;
    return dayCode;
} /* calcDay_Dec31 */

/**
*/
int InterlasaUtilities::dayInYear(int dd, int mm)
{
	switch(mm)
	{
		case 12:dd += 30;
        case 11:dd += 31;
        case 10:dd += 30;
        case 9:dd += 31;
        case 8:dd += 31;
        case 7:dd += 30;
        case 6:dd += 31;
        case 5:dd += 30;
        case 4:dd += 31;
        case 3:dd += 28;
        case 2:dd += 31;
    }
    return dd;
} /* dayInYear */

/**
*/
void InterlasaUtilities::nameInStr(char daysInWord[], int days,const PMString& Idioma)
{
	if(Idioma=="English" || Idioma=="Ingles")
	{
		switch(days)
   		{
   			case 0:strcpy(daysInWord, "Sunday");break;
			case 7:strcpy(daysInWord, "Sunday");break;
       		case 1:strcpy(daysInWord, "Monday");break;
       		case 2:strcpy(daysInWord, "Tuesday");break;
       		case 3:strcpy(daysInWord, "Wednesday");break;
     		case 4:strcpy(daysInWord, "Thursday");break;
        	case 5:strcpy(daysInWord, "Friday");break;
		    case 6:strcpy(daysInWord, "Saturday");break;
		}
	}
	else
	{
			if(Idioma=="Spanish" || Idioma=="Español")
			{
				switch(days)
   				 {
   				 	case 0:strcpy(daysInWord, "Domingo");break;
					case 7:strcpy(daysInWord, "Domingo");break;
       				case 1:strcpy(daysInWord, "Lunes");break;
       				case 2:strcpy(daysInWord, "Martes");break;
       				case 3:strcpy(daysInWord, "Miercoles");break;
     				case 4:strcpy(daysInWord, "Jueves");break;
        			case 5:strcpy(daysInWord, "Viernes");break;
			        case 6:strcpy(daysInWord, "Sabado");break;
			    }
			}
	}
	
} /* nameInStr */



bool16 InterlasaUtilities::ShowThePageByNumberPage(int32 which) 
{ 
	bool16 retval=kTrue;
	do
	{
		UID PageUID; 
		UID DocUID; 

		UIDRef PageRef; 
		UIDRef DocRef; 
		IDocument *DocPtr = Utils<ILayoutUIUtils>()->GetFrontDocument(); 
		IDataBase *theDB = GetDataBase(DocPtr); 

		InterfacePtr<ICommand> SetPgCmd(CmdUtils::CreateCommand(kSetPageCmdBoss)); 
		InterfacePtr<IControlView> view(Utils<ILayoutUIUtils>()->QueryFrontView()); 
		InterfacePtr<ILayoutControlData> LCD(view, UseDefaultIID()); 

		InterfacePtr<IPageList> PageList(DocPtr,IID_IPAGELIST); 
		PMString PageString; 
		PageString.AppendNumber(which); 
		PageUID = PageList->PageStringToUID(PageString); 
		if(PageUID == kInvalidUID)
		{ 
			//PostAlert("failed to move to the page--1"); 
			retval=kFalse; 
		} 

		//check here 
		PageRef = UIDRef(theDB,PageUID); 
		if (PageRef == UIDRef(nil, kInvalidUID))
		{ 
			//PostAlert("failed to move to the page--2"); 
			retval=kFalse; 
		} 
		InterfacePtr<IHierarchy> SpreadNode(PageRef,IID_IHIERARCHY); 
		SetPgCmd->SetItemList(UIDList(theDB,SpreadNode->GetSpreadUID())); 

		InterfacePtr<ILayoutCmdData> layoutData(SetPgCmd,IID_ILAYOUTCMDDATA); 
		DocUID = theDB->GetRootUID(); 
		DocRef = UIDRef(theDB,DocUID); 

		layoutData->Set(DocRef,LCD); 
		InterfacePtr<IBoolData> boolData(SetPgCmd,IID_IBOOLDATA); 
		boolData->Set(kFalse); 

		InterfacePtr<IUIDData> uidData(SetPgCmd,IID_IUIDDATA); 
		InterfacePtr<IGeometry> pageGeometry(PageRef, UseDefaultIID()); 
		uidData->Set(pageGeometry); 

		// Process the SetPageCmd: 
		if(CmdUtils::ProcessCommand(SetPgCmd) != kSuccess)
		{ 
			retval=kFalse; 	
		}
		else
			retval=kTrue; 
	}while(false);
	return(retval);
}


bool16 InterlasaUtilities::ObtenerNombreDAplicacion(PMString &NombreAplicacion)
{	
	bool16 retval=kTrue;
	do
	{
		InterfacePtr<IApplication> app(GetExecutionContextSession()->QueryApplication());
		if(app==nil)
			break;
		NombreAplicacion = app->GetApplicationName(); //Obtiene el nombre de la aplicacion InDesign/InCopy
		NombreAplicacion.SetTranslatable(kFalse);
	}while(false);
	return retval;
}

PMString InterlasaUtilities::ChangeEspanishDateStringToMySQLDateString(PMString &InDesignDate)
{	
    
    PMString retval=PMString("",PMString::kDontTranslateDuringCall);
    PMString Day=PMString("",PMString::kDontTranslateDuringCall);
    PMString Month=PMString("",PMString::kDontTranslateDuringCall);
    PMString Year=PMString("",PMString::kDontTranslateDuringCall);
    if(InDesignDate.NumUTF16TextChars()>6)
    {
        InDesignDate.Append("/");
        int32 Count=1;
        while(Count<=3)
        {
            switch(Count)
            {
                case 1:
                    Day=InDesignDate.GetItem("/",Count)->GetUTF8String().c_str();
                    break;
                case 2:
                    Month=InDesignDate.GetItem("/",Count)->GetUTF8String().c_str();
                    break;
                    
                case 3:
                    Year=InDesignDate.GetItem("/",Count)->GetUTF8String().c_str();
                    break;
            }
            Count++;
        }
        InDesignDate.Remove(InDesignDate.NumUTF16TextChars()-1,1);
        retval=Year+"-"+Month+"-"+Day;
    }
    else
    {
        retval="0000-00-00";
        //CAlert::InformationAlert(retval);
    }
    
    
    return retval;
}


PMString InterlasaUtilities::ChangeInDesignDateStringToMySQLDateString(PMString &InDesignDate)
{	
	
	PMString retval=PMString("",PMString::kDontTranslateDuringCall);
	PMString Day=PMString("",PMString::kDontTranslateDuringCall);
	PMString Month=PMString("",PMString::kDontTranslateDuringCall);
	PMString Year=PMString("",PMString::kDontTranslateDuringCall);
	if(InDesignDate.NumUTF16TextChars()>6)
	{
		InDesignDate.Append("/");
		int32 Count=1;
		while(Count<=3)
		{
			switch(Count)
			{
				case 1:
						Month=InDesignDate.GetItem("/",Count)->GetUTF8String().c_str();
						break;
				case 2:
						Day=InDesignDate.GetItem("/",Count)->GetUTF8String().c_str();
						break;
				case 3:
						Year=InDesignDate.GetItem("/",Count)->GetUTF8String().c_str();
						break;
			}
			Count++;
		}
		InDesignDate.Remove(InDesignDate.NumUTF16TextChars()-1,1);
		retval=Year+"-"+Month+"-"+Day;
	}
	else
	{
		retval="0000-00-00";
		//CAlert::InformationAlert(retval);
	}
	
	
	return retval;
}



PMString InterlasaUtilities::ChangeInDesignDateStringToDateStringYMD(PMString &InDesignDate)
{	
	PMString retval=PMString("",PMString::kDontTranslateDuringCall);
	
	
	PMString Day=PMString("",PMString::kDontTranslateDuringCall);
	PMString Month=PMString("",PMString::kDontTranslateDuringCall);
	PMString Year=PMString("",PMString::kDontTranslateDuringCall);
	if(InDesignDate.NumUTF16TextChars()>1)
	{
		InDesignDate.Append("/");
		int32 Count=1;
		while(Count<=3)
		{
			switch(Count)
			{
				case 1:
						Month=InDesignDate.GetItem("/",Count)->GetUTF8String().c_str();
						break;
				case 2:
						Day=InDesignDate.GetItem("/",Count)->GetUTF8String().c_str();
						break;
				case 3:
						Year=InDesignDate.GetItem("/",Count)->GetUTF8String().c_str();
						break;
			}
			Count++;
		}
		InDesignDate.Remove(InDesignDate.NumUTF16TextChars()-1,1);
		retval=Year+""+Month+""+Day;
	}
	
	
	return retval;
}

//PARA OBTENCION DEL PATH
PMString InterlasaUtilities::MacToUnix(PMString path, const bool16& scapeSpaces)
{
	PMString unixpath;
	const PMString slash = "/";
	const PMString invertidos="\\";
	if (path.Contains(":"))
	{  
		int posicion = path.IndexOfString(":",0);
		
		path.Remove(0,posicion);
		do
		{
			posicion = path.IndexOfString(":",0);
			if (posicion >= 0)
			{			
				path.Insert(slash,posicion,kMaxInt32);
				path.Remove(posicion+1,1);
				
			}
			
		}while(path.Contains(":"));
		
		if(scapeSpaces==kTrue){
            posicion = path.IndexOfString(" ",0);
            while(posicion>0)
            {
                PMString CSD=PMString("",PMString::kDontTranslateDuringCall);
                CSD.AppendNumber(posicion);
                //CAlert::InformationAlert("FUNCION MacToUnix  "+CSD+" "+path);
                path.Insert(invertidos,posicion,kMaxInt32);
                posicion = path.IndexOfString(" ",posicion+2);
            }
        }
		unixpath = path;
		
		
		//CAlert::InformationAlert("FUNCION MacToUnix  "+path);
	}
	else
	{
        if(scapeSpaces==kTrue){
            int posicion = path.IndexOfString(" ",0);
            while(posicion>0)
            {
                PMString CSD=PMString("",PMString::kDontTranslateDuringCall);
                CSD.AppendNumber(posicion);
                //CAlert::InformationAlert("FUNCION MacToUnix  "+CSD+" "+path);
                path.Insert(invertidos,posicion,kMaxInt32);
                posicion = path.IndexOfString(" ",posicion+2);
            }
        }
		unixpath = path;
	}
	
	return unixpath;	
}



PMString InterlasaUtilities::UnixToMac(PMString path)
{
	PMString unixpath;
	const PMString slash = ":";
	if (path.Contains("/"))
	{
		int posicion = path.IndexOfString("/",0);
		
		path.Remove(0,posicion);
		do
		{
			posicion = path.IndexOfString("/",0);
			if (posicion >= 0)
			{			
				path.Insert(slash,posicion,kMaxInt32);
				path.Remove(posicion+1,1);
			}
			
		}while(path.Contains("/"));
		
		unixpath = path;
		
	}
	
	return unixpath;	
}

PMString InterlasaUtilities::FormatFechaYHoraManana(PMString Format)
{
  time_t tiempo;
  char cad[80];
  struct tm *tmPtr;

  tiempo = time(NULL);
  tiempo = tiempo+86400;
  
  
 // setlocale(LC_ALL,"C");

  tmPtr = localtime(&tiempo);
  
  
  
 
  
 strftime( cad, 80, Format.GetUTF8String().c_str(), tmPtr );

 
  PMString Fecha(cad);
  Fecha.Translate();
  Fecha.SetTranslatable(kTrue);
  return Fecha;
}

PMString InterlasaUtilities::FormatFechaYHoraActual(PMString Format)
{
  time_t tiempo;
  char cad[80];
  struct tm *tmPtr;

  tiempo = time(NULL);
  tiempo = tiempo;
  
  
 // setlocale(LC_ALL,"C");

  tmPtr = localtime(&tiempo);
  
  
  
 
  
 strftime( cad, 80, Format.GetUTF8String().c_str(), tmPtr );

 
  PMString Fecha(cad);
  Fecha.Translate();
  Fecha.SetTranslatable(kTrue);
  return Fecha;
}

PMString InterlasaUtilities::RemoveCharactersReturn(const PMString& Ordinary)
{
	PMString fromthis = Ordinary;
	
	// we have a name at the start if the path. Need to remove it...			
	// first of all, count the number of characters to the first delimeter. 
	int delpos = 0;
	for (int i = 0; i<fromthis.CharCount(); i++)
	{
		PlatformChar character=fromthis[i];
		if( character.IsLineBreakChar())
		{
			delpos = i;
			fromthis.Remove(delpos,1);
			i = 0;
		}
	}
	return(fromthis);
}


ErrorCode InterlasaUtilities::SaveCopyOfDocumentAs(const UIDRef & 	documentUIDRef,	const IDFile & 	sysFile,UIFlags uiFlags)//,UIFlags uiFlags = kFullUI
{
	ErrorCode result = kFailure;
	do
	{
	 InterfacePtr<IDocFileHandler> docFileHandler(Utils<IDocumentUtils>()->QueryDocFileHandler(documentUIDRef));
         if (!docFileHandler) {
             break;
         }
         //Try to do SaveAs
         if(docFileHandler->CanSaveACopy(documentUIDRef) ) 
         {
         	// if(FileUtils::CanOpen(sysFile, FileUtils::kWrite))
         	// {
         	 	 docFileHandler->SaveACopy (documentUIDRef, &sysFile, uiFlags);
          	   result = ErrorUtils::PMGetGlobalErrorCode();
          	   ASSERT_MSG(result == kSuccess, "IDocFileHandler::SaveAs failed");
           	  if (result != kSuccess) 
           	  {
             	    break;
           	  }
         	// }
         	// else
         	// {
         	// 	//T>CAlert::InformationAlert("The BackUp File is read only.");
         	// }
            
         }
     } while(false);
     return result;
}




PMRect InterlasaUtilities::GetPageItemBoundingBox(const UIDRef &elem, PMReal *rotationAngle, PMReal *skewAngle  ) 
{
	PMRect theRect(0, 0, 0, 0 );
	PMMatrix matrizOri;
	bool bRevert(false );
	PMReal rotation(0.0 );
	PMReal SkewAngle(0.0 );	
 	do {
		IDataBase *database = elem.GetDataBase();
		if (database == NULL )
			break;		
		InterfacePtr<IGeometry> itemGeom(elem, IID_IGEOMETRY );
		if (itemGeom == NULL )
			break;
		
		PMRect innerBBox;
		InterfacePtr<ITransform> transf(itemGeom, IID_ITRANSFORM); //JCSM
		if (transf != NULL ) {
			PMPoint p(0,0);	
			PMRect innerBBoxInit = itemGeom->GetStrokeBoundingBox( );
			PMMatrix matriz = transf->GetInnerToParentMatrix();
			matrizOri = matriz;
			rotation = transf->GetItemRotationAngle();
			SkewAngle =	transf->GetItemSkewAngle();
			if (((rotation != 0 ) || (SkewAngle != 0 ) ) && (skewAngle != NULL ) ) { 
				{{
					PBPMPoint referencePoint = Utils<IRefPointUtils>()->CalculateReferencePoint_3(IReferencePointData::kCenterReferencePoint, UIDList( elem ) );
					InterfacePtr<IGeometry> itemGeom3(elem, IID_IGEOMETRY );
					if (itemGeom3 != NULL ) {
						InterfacePtr<ITransform> transf3(itemGeom3, IID_ITRANSFORM ); //JCSM
						if (transf3 != NULL ) {
						//	//T>CAlert::InformationAlert("ITransformCmdUtils");
						if (rotation != 0 )		
							Utils<Facade::ITransformFacade>()->TransformItems(UIDList(elem ), Transform::PasteboardCoordinates(), 
																									referencePoint, 
																									Transform::RotateTo(0.0 ) );

						if (SkewAngle != 0 )
							Utils<Facade::ITransformFacade>()->TransformItems(UIDList(elem ), Transform::PasteboardCoordinates(), 
																									referencePoint, 
																									Transform::SkewTo(0.0 ) );
							
/*						
							if (rotation != 0 )
								Utils<ITransformCmdUtils>()->DoRotate(transf3,
																		referencePoint, 
																		0.0,															   
																		kTrue );
							if (SkewAngle != 0 )
								Utils<ITransformCmdUtils>()->DoSkew(transf3,
																		referencePoint,
																		0.0, 
																		0.0,															   
																		kTrue );
*/																		
						}					
					}
				}}
				bRevert = true;
				InterfacePtr<IGeometry> itemGeomNewGeo(elem, IID_IGEOMETRY );					
				innerBBox = itemGeomNewGeo->GetStrokeBoundingBox( );
			} else
				innerBBox = itemGeom->GetStrokeBoundingBox( );
			if (rotationAngle != NULL )
				*rotationAngle = rotation;
			if (skewAngle != NULL )
				*skewAngle = SkewAngle;
			
		} else
			innerBBox = itemGeom->GetStrokeBoundingBox( );
		
		PMPoint A(innerBBox.LeftBottom());
		PMPoint B(innerBBox.Right(), innerBBox.Bottom());
		PMPoint C(innerBBox.RightTop());
		PMPoint D(innerBBox.Left(), innerBBox.Top());
		InterfacePtr<IHierarchy> itemHier(elem, IID_IHIERARCHY);
		if (itemHier == NULL )
			break;
		UID pageUID = Utils<ILayoutUtils>()->GetOwnerPageUID(itemHier);
		InterfacePtr<IGeometry> pageGeom(database, pageUID, IID_IGEOMETRY);
		if (pageGeom == NULL )
			break;
		InterfacePtr<IHierarchy> spreadHier(itemHier->QueryRoot() );
		if (spreadHier == NULL )
			break;
		PMMatrix page2Spread;
		::InnerToArbitraryParentMatrix(&page2Spread, pageGeom, spreadHier );
		PMMatrix item2Spread;
		::InnerToArbitraryParentMatrix(&item2Spread, itemGeom, spreadHier );
/*		
		{//Quitar si hay fracaso.
			PMReal 	itemX = item2Spread.GetXTranslation();
			PMReal 	itemY = item2Spread.GetYTranslation();
			PMReal 	rotation = item2Spread.GetRotationAngle ();
			PMReal 	skew = item2Spread.GetXSkewAngle ();
			if ((rotation != 0 ) || (skew != 0 ) ) {
				item2Spread.SetOrigin(0, 0);
				item2Spread.SkewTo(0);
				item2Spread.RotateTo(0);
				item2Spread.SetOrigin(itemX, itemY );							
			}			
		}
*/		
		
		PMMatrix item2Page = item2Spread;
		PMMatrix spread2Page = page2Spread;
		spread2Page.Invert();
		item2Page.PostConcat(spread2Page ); // item2Spread*Spread2Page cambiar aqui el angulo		
/*		
		{//Quitar si hay fracaso.
			PMReal 	itemX = item2Page.GetXTranslation();
			PMReal 	itemY = item2Page.GetYTranslation();
			PMReal 	rotation = item2Page.GetRotationAngle ();
			PMReal 	skew = item2Page.GetXSkewAngle ();
			if ((rotation != 0 ) || (skew != 0 ) ) {
				item2Page.SetOrigin(0, 0);
				item2Page.SkewTo(0);
				item2Page.RotateTo(0);
				item2Page.SetOrigin(itemX, itemY );							
			}			
		}
*/
		item2Page.Transform(&A );
		item2Page.Transform(&B );
		item2Page.Transform(&C );
		item2Page.Transform(&D );

		theRect.Left(D.X() ); 
		theRect.Top(D.Y() );	 
		theRect.Right(B.X() );	 
		theRect.Bottom(B.Y() );
        
        PMString SCS="";
        SCS.AppendNumber(theRect.Left());
        SCS.Append(",");
        SCS.AppendNumber(theRect.Top());
        SCS.Append(",");
        SCS.AppendNumber(theRect.Right());
        SCS.Append(",");
        SCS.AppendNumber(theRect.Bottom());
        //CAlert::InformationAlert(SCS);
        
		if (theRect.Left() > theRect.Right() ) {
			theRect.Left(B.X() ); 
			theRect.Right(D.X() );	 
		}
		if (theRect.Top() > theRect.Bottom() ) {
			theRect.Bottom(D.Y() ); 
			theRect.Top(B.Y() );	 
		}

	} while(kFalse ) ;
    
	if (bRevert ) {
				{{
					PBPMPoint referencePoint = Utils<IRefPointUtils>()->CalculateReferencePoint_3(IReferencePointData::kCenterReferencePoint, 
																													UIDList(elem ) );	
					InterfacePtr<IGeometry> itemGeom3(elem, IID_IGEOMETRY );
					if ( itemGeom3 != NULL ) {
						InterfacePtr<ITransform> transf3(itemGeom3, IID_ITRANSFORM ); //JCSM
						if ( transf3 != NULL ) {
//						//T>CAlert::InformationAlert("ITransformCmdUtils");
						if ( rotation != 0 )		
							Utils<Facade::ITransformFacade>()->TransformItems(UIDList(elem ), Transform::PasteboardCoordinates(), 
																									referencePoint, 
																									Transform::RotateTo(-1 * rotation ) );

						if (SkewAngle != 0 )
							Utils<Facade::ITransformFacade>()->TransformItems(UIDList(elem ), Transform::PasteboardCoordinates(), 
																									referencePoint, 
																									Transform::SkewTo(-1 * SkewAngle ) );

/*						
							if (rotation != 0 )
								Utils<ITransformCmdUtils>()->DoRotate(transf3,
																		referencePoint, 
																		-1 * rotation,															   
																		kTrue );
							if (SkewAngle != 0 )
								Utils<ITransformCmdUtils>()->DoSkew(transf3,
																		referencePoint,
																		-1 * SkewAngle, 
																		0.0,															   
																		kTrue );
*/																		
						}					
					}
				}}
		
	}
		//SetMatrix(elem, matrizOri );
	return theRect;
}


/*
	Funcion para obtener llenar la lista de Link a actualizar
*/
 bool16 InterlasaUtilities::GetListaDLinksParaActualizar( const InterfacePtr< IHierarchy > & FrameHierarchy,UIDList& uidsToUpdate)
{
	bool16 retval=kFalse;
	do
	{
		
		/*InterfacePtr<IHierarchy> InLineHierarchy(FrameHierarchy, UseDefaultIID()); 
		ASSERT(InLineHierarchy);
		if(!InLineHierarchy) 
		{
			break;
		}*/
		
		InterlasaUtilities::RecursiveHierarchyGetUpdateListLinks( FrameHierarchy,uidsToUpdate);
									 									
	}while(false);
	
	
	//CAlert::InformationAlert("Cadena:"+StringCell);
	return retval;
}


/*
	Funcion Recursiva para navegas sobre la gerarquia PageItem para llenar la Lista de Links a Actualizar
*/
bool16 InterlasaUtilities::RecursiveHierarchyGetUpdateListLinks( const InterfacePtr< IHierarchy > & hierarchyItem, UIDList& uidsToUpdate)
{
	do
	{
		int32 s=0;
		while(s<hierarchyItem->GetChildCount())
		{
			InterfacePtr<IHierarchy> childHierarchyFrame(hierarchyItem->QueryChild(s)); 
			if(childHierarchyFrame)
			{
				InterlasaUtilities::AddLinkToUpdateList(childHierarchyFrame, uidsToUpdate);
				//CAlert::InformationAlert("CadenaRefresada:"+StringCell);
				InterlasaUtilities::RecursiveHierarchyGetUpdateListLinks( childHierarchyFrame,uidsToUpdate);
			}
			s++;
		}

	}while(false);
	
	return kTrue;
}

/*
	Adiciona a la lista de Link a actualizar si es que lo necesita.
*/
bool16 InterlasaUtilities::AddLinkToUpdateList(const InterfacePtr< IHierarchy > & imageHierarchy, UIDList& uidsToUpdate)
{
	do
	{
		InterfacePtr<ILinkObject> iLinkObject(imageHierarchy,UseDefaultIID()); 
		if(!iLinkObject)
		{
			break;
		}
		
		PMString SAW=PMString("",PMString::kDontTranslateDuringCall);
		  
		// if(iLinkObject->GotoLinkedObject(&SAW)!=kSuccess)
		//	CAlert::InformationAlert(SAW);
			
		// get the link for this object 
		IDataBase* iDataBase = ::GetDataBase(imageHierarchy); 
		InterfacePtr<ILinkManager> iLinkManager(iDataBase,iDataBase->GetRootUID(),UseDefaultIID()); 
		if(!iLinkManager)
		{
			break;
		}
				
		ILinkManager::QueryResult linkQueryResult; 
		if (iLinkManager->QueryLinksByObjectUID(::GetUID(imageHierarchy), linkQueryResult)) 
		{ 
			ASSERT_MSG(linkQueryResult.size()==1,"Only expecting single link with this object"); 
			ILinkManager::QueryResult::const_iterator iter = linkQueryResult.begin(); 
			InterfacePtr<ILink> iLink (iDataBase, *iter,UseDefaultIID()); 
			if (iLink!=nil) 
			{ 
				//CAlert::InformationAlert("si es un link valido");
				if (iLink->GetResourceModificationState() == ILink::kResourceModified )
				{
					uidsToUpdate.push_back(*iter);
				}
			} 
			
		} 
		

	}while(false);
return kTrue;
}



bool16 InterlasaUtilities::getTextOfWidgetFromPalette(const WidgetID& PaleteWidgetID,	const WidgetID& widgetID, PMString& value)
{
	bool16 retval=kTrue;
	do
	{
		InterfacePtr<IPanelControlData>	panel(Utils<IPalettePanelUtils>()->QueryPanelByWidgetID(PaleteWidgetID)) ;
		if(panel==nil)
		{
			CAlert::InformationAlert(" InterlasaUtilities::getTextOfWidgetFromPalette  No se encontro el editbox");
			break;
		}
		
		IControlView*	iControView= panel->FindWidget(widgetID);//, UseDefaultIID()
		if(iControView==nil)
		{
			//CAlert::InformationAlert("  InterlasaUtilities::getTextOfWidgetFromPalette iControViewList");
			break;
		}
		
		
		
		//obtiene una interface tel tipo ITextControlData para obtener el texto de esta caja o Widget
		InterfacePtr<ITextControlData>	TextoDeDireccion( iControView, UseDefaultIID());
		if(TextoDeDireccion==nil)
		{
			//CAlert::InformationAlert("No se encontro el texto de direccion");
			break;
		}
		
		value = TextoDeDireccion->GetString();
		retval=kTrue;
		
	}while(false);
	return(retval);
}


bool16 InterlasaUtilities::setTextOfWidgetFromPalette(const WidgetID& PaleteWidgetID,	const WidgetID& widgetID, const PMString& value)
{
	bool16 retval=kTrue;
	do
	{
		InterfacePtr<IPanelControlData>	panel(Utils<IPalettePanelUtils>()->QueryPanelByWidgetID(PaleteWidgetID)) ;
		if(panel==nil)
		{
			CAlert::InformationAlert("  InterlasaUtilities::setTextOfWidgetFromPalette  No se encontro el editbox");
			break;
		}
		
		IControlView*	iControView = panel->FindWidget(widgetID);//, UseDefaultIID() 
		if(iControView==nil)
		{
			//T>CAlert::InformationAlert("iControView==nil interlasautilities linea 2121");
			break;
		}
		
		
		
		//obtiene una interface tel tipo ITextControlData para obtener el texto de esta caja o Widget
		InterfacePtr<ITextControlData>	TextoDeDireccion( iControView, UseDefaultIID());//
		if(TextoDeDireccion==nil)
		{
			//T>CAlert::InformationAlert("No se encontro el texto de direccion");
			break;
		}
		
		 TextoDeDireccion->SetString(value);
		retval=kTrue;
		
	}while(false);
	return(retval);
}

bool16 InterlasaUtilities::IsShowWidgetFromPalette(const WidgetID& PaleteWidgetID,	const WidgetID& widgetID)
{
	bool16 retval=kTrue;
	do
	{
		InterfacePtr<IPanelControlData>	panel(Utils<IPalettePanelUtils>()->QueryPanelByWidgetID(PaleteWidgetID)) ;
		if(panel==nil)
		{
			CAlert::InformationAlert("  InterlasaUtilities::IsShowWidgetFromPalette  No se encontro el editbox");
			break;
		}
		
		IControlView*	iControView = panel->FindWidget(widgetID);//, UseDefaultIID()
		if(iControView==nil)
		{
			//T>CAlert::InformationAlert("InterlasaUtilities linea 2157");
			break;
		}
		
		retval=	iControView->IsVisible();
		
	}while(false);
	return(retval);
}


/*
	bool16 InterlasaUtilities::HideOrShowWidgetFromPalette(const WidgetID& PaleteWidgetID,	const WidgetID& widgetID,  bool16 mostrar)
 */
bool16 InterlasaUtilities::HideOrShowWidgetFromPalette(const WidgetID& PaleteWidgetID,	const WidgetID& widgetID,  bool16 mostrar)
{
	bool16 retval=kTrue;
	do
	{
		InterfacePtr<IPanelControlData>	panel(Utils<IPalettePanelUtils>()->QueryPanelByWidgetID(PaleteWidgetID)) ;
		if(panel==nil)
		{
			CAlert::InformationAlert("  InterlasaUtilities::HideOrShowWidgetFromPalette  No se encontro el editbox");
			break;
		}
		
		IControlView*	iControView = panel->FindWidget(widgetID);//, UseDefaultIID() 
		if(iControView==nil)
		{
			//T>CAlert::InformationAlert("iControView==nil InterlasaUtilities linea 2186");
			break;
		}
		
		
		
		if(mostrar)
			iControView->ShowView(kTrue);
		else
			iControView->HideView();
		
	}while(false);
	return(retval);
}

bool16 InterlasaUtilities::EnabledOrDisabledWidgetFromPalette(const WidgetID& PaleteWidgetID,	const WidgetID& widgetID,  bool16 Enabled)
{
	bool16 retval=kTrue;
	do
	{
		InterfacePtr<IPanelControlData>	panel(Utils<IPalettePanelUtils>()->QueryPanelByWidgetID(PaleteWidgetID)) ;
		if(panel==nil)
		{
			CAlert::InformationAlert("  InterlasaUtilities::EnabledOrDisabledWidgetFromPalette  No se encontro el editbox");
			break;
		}
		
		IControlView*	iControView= panel->FindWidget(widgetID);//, UseDefaultIID()
		if(iControView==nil)
		{
			//CAlert::InformationAlert(" iControView==nil InterlasaUtilities linea 2216 ");
			break;
		}
		
		if(Enabled)
			iControView->Enable();
		else
			iControView->Disable();
				
	}while(false);
	return(retval);
}


/* GetPageItemType
 */
void InterlasaUtilities::GetPageItemType(const UIDRef& pageItemUIDRef, PMString& pageItemType)
{
	do
	{
		if (Utils<IPageItemTypeUtils>()->IsGraphicFrame(pageItemUIDRef) == kTrue)
		{
			pageItemType = "GraphicFrame";
		}
		else if (InterlasaUtilities::IsEmptyGraphicFrame(pageItemUIDRef) == kTrue )
		{
			pageItemType = "EmptyGraphicFrame";
		}
		else if (Utils<IPageItemTypeUtils>()->IsTextFrame(pageItemUIDRef) == kTrue)
		{
			pageItemType = "TextFrame";
		}
		else if (InterlasaUtilities::IsGroup(pageItemUIDRef) == kTrue)
		{
			pageItemType = "Group";
		}
		else if (Utils<IPageItemTypeUtils>()->IsInline(pageItemUIDRef) == kTrue)
		{
			pageItemType = "Inline";
		}
		else if (Utils<IPageItemTypeUtils>()->IsGraphic(pageItemUIDRef) == kTrue)
		{
			pageItemType = "Graphic";//pageItemType = "TextFrame";pageItemType = "EmptyGraphicFrame";pageItemType = "GraphicFrame";
		}
		else if (Utils<IPageItemTypeUtils>()->IsTextOnAPath(pageItemUIDRef) == kTrue)
		{
			pageItemType = "TextOnAPath";
		}
		else if (Utils<IPageItemTypeUtils>()->IsStandOff(pageItemUIDRef) == kTrue)
		{
			pageItemType = "StandOff";
		}
		else if (Utils<IPageItemTypeUtils>()->IsGuide(pageItemUIDRef) == kTrue)
		{
			pageItemType = "Guide";
		}
		else if (InterlasaUtilities::IsSplineShape(pageItemUIDRef, pageItemType) == kTrue)
		{
			// see if we can determine if it is a spline, and what shape it is...
			// we know it is a spline...
			pageItemType.Append(" spline");
		}
		else
		{
			pageItemType = "Unknown";
		}
	} while (false);
}


bool16 InterlasaUtilities::IsEmptyGraphicFrame(const UIDRef& itemUIDRef)
{
	bool16 result = kFalse;
	do
	{
		// Graphic frames have an IGraphicFrameData.
		InterfacePtr<IGraphicFrameData> graphicFrameData(itemUIDRef, UseDefaultIID());
		if (graphicFrameData == nil)
			break;
		if (graphicFrameData->IsGraphicFrame() != kTrue)
			break;
		
		// Empty graphic frames don't have content.
		if (graphicFrameData->HasContent() == kTrue)
			break;
		
		// OK it's an empty graphic frame.
		result = kTrue;
	} while (false);
	return result;
}


/* GetShape
 */
bool16 InterlasaUtilities::IsSplineShape(const UIDRef& itemUIDRef, PMString& shape)
{
	bool16 result = kFalse;
	InterfacePtr<IPathGeometry> pathGeometry(itemUIDRef,UseDefaultIID());
	if (pathGeometry != nil)
	{
		result = kTrue;
		PMPageItemType pathType = Utils<IPathUtils>()->WhichKindOfPageItem(pathGeometry);
		switch (pathType)
		{
			case kIsLine:
				shape = "Line";
				break;
			case kIsRectangle:
				shape = "Rectangle";
				break;
			case kIsCircle:
				shape = "Circle";
				break;
			case kIsOval:
				shape = "Oval";
				break;
			/*case kIsRegularPoly:
				shape = "RegularPoly";
				break;
			
			 case kIsIrregularPoly:
				shape = "IrregularPoly";
				break;
			*/
			 case kIsSquare:
				shape = "Square";
				break;
			default:
				shape = "Unknown";
				break;
		}
	}
	return result;
}

/* IsGroup
 */
bool16 InterlasaUtilities::IsGroup(const UIDRef& itemUIDRef)
{
	bool16 result = kFalse;
	do
	{
		// Groups have a hierarchy.
		InterfacePtr<IHierarchy> hierarchy(itemUIDRef, UseDefaultIID());
		if (hierarchy == nil)
			break;
		
		// Groups don't have an IGraphicFrameData
		InterfacePtr<IGraphicFrameData> graphicFrameData(hierarchy, UseDefaultIID());
		if (graphicFrameData != nil)
			break;
		
		// Groups have more than one child.
		if (hierarchy->GetChildCount() <= 1)
			break;
		
		// OK it's a group.
		result = kTrue;
	} while (false);
	return result;
}


bool16 InterlasaUtilities::SetRrscEnWidgetFromPalette(const WidgetID& PaleteWidgetID,	const WidgetID& widgetID,  const RsrcID &id)
{

	bool16 retval=kTrue;
	do
	{
		InterfacePtr<IPanelControlData>	panel(Utils<IPalettePanelUtils>()->QueryPanelByWidgetID(PaleteWidgetID)) ;
		if(panel==nil)
		{
			CAlert::InformationAlert("  InterlasaUtilities::SetRrscEnWidgetFromPalette  No se encontro el editbox");
			break;
		}
		
		IControlView*	iControView= panel->FindWidget(widgetID);//, UseDefaultIID()
		if(iControView==nil)
		{
			//CAlert::InformationAlert("iControView==nil InterlasaUtilities iControViewList linea 2397");
			break;
		}
		
		iControView->SetRsrcID(id);
		
	}while(false);
	return(retval);
}


PMString InterlasaUtilities::Converter(char * web_path)//This function is for converting the incoming messages from utf8 to utf16 so a PMString can be used
{
	PMString retval = "";
#if defined(MACINTOSH)
	CFStringRef theString = CFStringCreateWithCString(NULL,web_path,kCFStringEncodingMacRoman);
	CFIndex length = CFStringGetLength(theString);
	CFRange range = CFRangeMake(0,length);
	UniChar buffer[length];
	CFStringGetCharacters ( theString, range, buffer);
	return PMString((UTF16TextChar *)buffer,length);
#elif defined(WINDOWS)

	retval = PMString(web_path);
#endif
	return retval;
} 

PMString InterlasaUtilities::GetFolderLibraryOfUser()
{
	PMString retval="";
#if defined(MACINTOSH)
	PMString appafolder="";
	SDKUtilities::GetApplicationFolder(appafolder); //ORIGINAL
	IDFile result;
	FileUtils::PMStringToIDFile(appafolder,result);
	
	PMString volumeName="";
	FileUtils::GetVolumeName(&result, &volumeName);
	
	const char *homeDir = getenv("HOME");		
	PMString Archivo=PMString(PMString(homeDir)+"/Library");
	
	
	PMString path2MAC=InterlasaUtilities::UnixToMac(Archivo);
	volumeName.Append(path2MAC);
	retval=volumeName;
#elif defined(WINDOWS)
	
	retval="";
	#endif
	
	return retval;
	
}

PMString InterlasaUtilities::GetFolderLibraryOfShared()
{
	PMString retval="";
#if defined(MACINTOSH)
	PMString appafolder="";
	SDKUtilities::GetApplicationFolder(appafolder); //ORIGINAL
	IDFile result;
	FileUtils::PMStringToIDFile(appafolder,result);
	
	PMString volumeName="";
	FileUtils::GetVolumeName(&result, &volumeName);
	
	const char *homeDir = getenv("HOME");		
	PMString Archivo=PMString(PMString(homeDir)+"/Library");
	
	
	PMString path2MAC=InterlasaUtilities::UnixToMac(Archivo);
	volumeName.Append(path2MAC);
	
	//CAlert::InformationAlert(volumeName);
	CharCounter index=volumeName.LastIndexOfCharacter(PlatformChar(':'));
	volumeName.Remove(index,kMaxInt32);
	//CAlert::InformationAlert(volumeName);
	index=volumeName.LastIndexOfCharacter(PlatformChar(':'));
	volumeName.Remove(index,kMaxInt32);
	//CAlert::InformationAlert(volumeName);
	volumeName.Append(":Shared:Library");
	//CAlert::InformationAlert(volumeName);
	retval=volumeName;
#elif defined(WINDOWS)
	
	retval="";
#endif
	
	return retval;
	
}

/**
 Funcion que crea una determinada capa.
 
 
 @param NombredeCapa, Variable del tipo PMString que contiene el nombre de la capa con que se debe crear.
 @param CapaBloqueada,	Variable entero que indicara un valor p¬¨‚àëra determinar el estado de bloqueo de la capa a crear;0 para no
 bloqueada, 1 para bloqueada.
 @param CapaVisible,	Variable entero que indicara un valor p¬¨‚àëra determinar el estado de muestra de la capa a crear; 0 para no
 visible, 1 para visible
 
 
 */

void InterlasaUtilities::CrearCapa( PMString NombredeCapa, int32 CapaBloqueada, int32 CapaVisible )

{	ErrorCode error = kCancel;
    
	UID LayerUID = kInvalidUID;
    
    // We'll use a do-while(false) to break out on bad pointers:
	do
    {
        IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (document == nil)
		{
			//CAlert::WarningAlert("Error al intentar Crear nueva Capa");
			break;
		}
        InterfacePtr<ILayerList> layerList(document, IID_ILAYERLIST);
        if (layerList == nil)
        {
        	ASSERT_FAIL("AddLayerButtonWidgetObserver::Update: layerList invalid");
        	//CAlert::ErrorAlert("Error al intentar Crear nueva Capa");
        	break;
        }
        //Busca de una capa por nombre.
        LayerUID=layerList->FindByName(NombredeCapa);
        if(LayerUID!=kInvalidUID)//salir en caso de que exista esta capa
        	break;
       
        InterfacePtr<ICommand> newLayerCmd(CmdUtils::CreateCommand(kNewLayerCmdBoss));
        if (newLayerCmd == nil)
        {
            ASSERT_FAIL("could not create newLayerCmd");
            break;
        }
        
		InterfacePtr<INewLayerCmdData> newLayerData(newLayerCmd, IID_INEWLAYERCMDDATA);
		if (newLayerData == nil)
        {
			ASSERT_FAIL("newLayerData invalid");
			break;
        }
        
        newLayerData->Set(GetUIDRef(document), &NombredeCapa);
        
		error = CmdUtils::ProcessCommand(newLayerCmd);
        if (error != kSuccess)
        {
        	ASSERT_FAIL("newLayerCmd failed");
        	break;
        }
        
        UIDList newLayerUIDList = UIDList(*newLayerCmd->GetItemList());
        
		// Make the new layer the active layer:
		InterfacePtr<ICommand> setActiveLayerCmd(CmdUtils::CreateCommand(kSetActiveLayerCmdBoss));
		if (setActiveLayerCmd == nil)
		{
			ASSERT_FAIL("could not create setActiveLayerCmd");
			break;
		}
	}while(false); // Only do once.
	// Otherwise we never were able to start the sequence.
}

/**
 
 Activa una capa dependiendo su nombre NomCapa
 @param NombredeCapa, Variable del tipo PMString que contiene el nombre de la capa que se desea activar.
 */
bool16 InterlasaUtilities::ActivarCapa(PMString NomCapa)
{
    
	bool16 retval = kFalse;
	ErrorCode error=kCancel;
    
	do
        
	{
        
		//obtiene la interfaz IDocument del documento actual
        
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
        
		if (document == nil)
            
		{
            
			break;
            
		}
        
		
        
		//obtiene el UIDRef del documento
        
		const UIDRef docUIDRef = ::GetUIDRef(document);
        
        
		//obtien la interfaz ILayerList o lista de de capas del documento actual
        
		InterfacePtr<ILayerList> layerList(document, IID_ILAYERLIST);
        
		if (layerList == nil)
            
		{
            
			ASSERT_FAIL("AddLayerButtonWidgetObserver::Update: layerList invalid");
            
			break;
            
		}
        
		//Busqueda de la capa por su nombre obtienendo su UID
        
		UID Capa= layerList->FindByName(NomCapa);
        
		if(Capa==kInvalidUID)
            
		{
            
			break;
            
		}
		//obtiene el numero de capa que se desea activar a partir de su UID
        int32 NumLayerAActivar=layerList->GetLayerIndex(Capa);
        if(NumLayerAActivar<0)
			break;
        
		//se obtien la interfaz IDocumentLayer a partir del numero de capa
        IDocumentLayer* documentLayer = layerList->QueryLayer(NumLayerAActivar);
		
        
		// Now we've created the new layer, we must set it as the active layer:
        
		InterfacePtr<ICommand> setActiveLayerCmd(CmdUtils::CreateCommand(kSetActiveLayerCmdBoss));
        
		
        
		InterfacePtr<ILayoutCmdData> layoutCmdData(setActiveLayerCmd, UseDefaultIID());
        
        
		InterfacePtr<ILayoutControlData> layoutControlData(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());
        
        if (setActiveLayerCmd == nil || layoutCmdData == nil || layoutControlData == nil)
            
		{
            
			ASSERT_FAIL("AddLayerButtonWidgetObserver::Update: setActiveLayerCmd, layoutCmdData, or layoutControlData invalid");
            
			break;
            
		}
        
		
        
		setActiveLayerCmd->SetItemList(UIDList(documentLayer));
		layoutCmdData->Set(docUIDRef, layoutControlData);
		// Process the command
        error = CmdUtils::ProcessCommand(setActiveLayerCmd);
		
		if (error != kSuccess)//si no se efectuo satisfactoriamente el comando
		{
            ASSERT_FAIL("AddLayerButtonWidgetObserver::Update: setActiveLayerCmd failed");
            break;
		}
        
		retval = kTrue;
        
	}while(false);
    
	return(retval);
    
}



/*bool16 InterlasaUtilities::GetItemFromPMString(PMString source,	PMString item, int32 numitem, PMString &itemcontent)
{
	bool16 retval=kFalse;
	
	
	
}*/


PMString InterlasaUtilities::normalizeWordForPunctuationANDUTF8(const PMString &SPCallPaginnaChickIn)
{
    //Quita caracteres de control
   
/*  Comente esta funcion pues al parecer ya no es necesaria pero veamos si es cierto con los acentos de las notas  
    WideString inserthis(SPCallPaginnaChickIn.GetUTF8String().c_str());
    InterlasaUtilities::NormalizeWordForPunctuation(inserthis);
    
    std::string msg;
    StringUtils::ConvertWideStringToUTF8(inserthis, msg);
    
    std::string msg2 = InterlasaUtilities::replaceAll( msg, "'", "'");
    
    PMString retval="";
    retval.Append(msg2.c_str());
    */
    return SPCallPaginnaChickIn;
}


void InterlasaUtilities::NormalizeWordForPunctuation(WideString &tempText)
{
    int32 numberOfCharacters = tempText.CharCount();
    for (int32 i = 0 ; i < numberOfCharacters; i++)
    {
        UTF32TextChar thisCharacter = tempText[i];
        if (thisCharacter == kTextChar_RightSingleQuotationMark ||
            thisCharacter == kTextChar_LeftSingleQuotationMark)
        {
            tempText.SetChar(i, kTextChar_Apostrophe);
        }
        else if (thisCharacter == kTextChar_LeftDoubleQuotationMark ||
                 thisCharacter == kTextChar_RightDoubleQuotationMark)
        {
            tempText.SetChar(i,  kTextChar_QuotationMark);
        }
    }
}

std::string InterlasaUtilities::replaceAll(std::string stringToModify,
                                           std::string charsToReplace,
                                           std::string replacementChars)
{
    
    std::string this_string = stringToModify;
    
    std::size_t this_occurrence = this_string.find(charsToReplace);
    while (this_occurrence != std::string::npos)
    {
        this_string.erase(this_occurrence, charsToReplace.size());
        this_string.insert(this_occurrence, replacementChars);
        this_occurrence = this_string.find(charsToReplace,
                                           this_occurrence + replacementChars.size());
    }
    
    return this_string;
}



int32 InterlasaUtilities::CountItems( PMString origen, PMString  delimited)
{
    int32 count=0;
    int32 position = origen.IndexOfString(delimited, 0);//IndexOfString(Target);
    int32 posicionFinalComa=position;
    while(position>=0)
    {
        count++;
        position = origen.IndexOfString(delimited, position+1);
        if (position >= 0) {
            posicionFinalComa=position;
        }
    }
    
    if(posicionFinalComa<origen.NumUTF16TextChars())
    {
        count++;
    }
    /*PMString SS="";
     SS.Append(origen+": ");
     SS.AppendNumber(count);
     CAlert::InformationAlert(SS);*/
    return count;
}

PMString InterlasaUtilities::GetIPActual()
{
    struct ifaddrs * ifAddrStruct=NULL;
    struct ifaddrs * ifa=NULL;
    void * tmpAddrPtr=NULL;
	PMString IPAddress="";
#if defined(MACINTOSH)   
    getifaddrs(&ifAddrStruct);
    
   
    for (ifa = ifAddrStruct; ifa != NULL; ifa = ifa->ifa_next)
    {
        
        if (!ifa->ifa_addr) {
            continue;
        }
        
        if (ifa->ifa_addr->sa_family == AF_INET)
        {   // check it is IP4
            // is a valid IP4 Address
            tmpAddrPtr=&((struct sockaddr_in *)ifa->ifa_addr)->sin_addr;
            char addressBuffer[INET_ADDRSTRLEN];
            inet_ntop(AF_INET, tmpAddrPtr, addressBuffer, INET_ADDRSTRLEN);
            
            IPAddress=addressBuffer;
        } 
    }
#elif defined(WINDOWS)
	/* Declare and initialize variables */
	DWORD dwSize = 0;
	DWORD dwRetVal = 0;

	unsigned int i = 0;
	// Set the flags to pass to GetAdaptersAddresses
	ULONG flags = GAA_FLAG_INCLUDE_PREFIX;

	// default to unspecified address family (both)
	ULONG family = AF_UNSPEC;

	LPVOID lpMsgBuf = NULL;

	PIP_ADAPTER_ADDRESSES pAddresses = NULL;
	ULONG outBufLen = 0;
	ULONG Iterations = 0;
	PIP_ADAPTER_ADDRESSES pCurrAddresses = NULL;
	PIP_ADAPTER_UNICAST_ADDRESS pUnicast = NULL;
	PIP_ADAPTER_ANYCAST_ADDRESS pAnycast = NULL;
	PIP_ADAPTER_MULTICAST_ADDRESS pMulticast = NULL;
	IP_ADAPTER_DNS_SERVER_ADDRESS *pDnServer = NULL;
	IP_ADAPTER_PREFIX *pPrefix = NULL;

	//family = AF_IPX;// AF_INET;

	// Allocate a 15 KB buffer to start with.
	outBufLen = WORKING_BUFFER_SIZE;
	do {
		pAddresses = (IP_ADAPTER_ADDRESSES *)MALLOC(outBufLen);
		if (pAddresses == NULL) {
			CAlert::InformationAlert("Memory allocation failed for IP_ADAPTER_ADDRESSES struct\n");
			exit(1);
		}

		dwRetVal =
			GetAdaptersAddresses(family, flags, NULL, pAddresses, &outBufLen);

		if (dwRetVal == ERROR_BUFFER_OVERFLOW) {
			FREE(pAddresses);
			pAddresses = NULL;
		}
		else {
			break;
		}
		Iterations++;

	} while ((dwRetVal == ERROR_BUFFER_OVERFLOW) && (Iterations < MAX_TRIES));
	if (dwRetVal == NO_ERROR) {

		// If successful, output some information from the data we received
		pCurrAddresses = pAddresses;
		bool16 salir = kFalse;
		while (pCurrAddresses && salir==kFalse) {

			PMString texto = "";
			texto.Append("IfIndex (IPv4 interface): ");
			texto.AppendNumber(pCurrAddresses->IfIndex);
			//texto.Append(WideString(pCurrAddresses->IfIndex));
			texto.Append("	\nAdapter name: ");
			texto.Append(WideString(pCurrAddresses->AdapterName));
	
			pUnicast = pCurrAddresses->FirstUnicastAddress;
			if (pUnicast != NULL) {
				for (i = 0; pUnicast != NULL; i++)
					pUnicast = pUnicast->Next;
				texto.Append("	\nNumber of Unicast Addresses : ");
				texto.AppendNumber(i);
			}
			//else
			//	CAlert::InformationAlert("\tNo Unicast Addresses\n");

			pAnycast = pCurrAddresses->FirstAnycastAddress;
			if (pAnycast) {
				for (i = 0; pAnycast != NULL; i++)
					pAnycast = pAnycast->Next;
				texto.Append("	\nNumber of Anycast Addresses : ");
				texto.AppendNumber(i);
			}
			//else
			//	CAlert::InformationAlert("\tNo Anycast Addresses\n");

			pMulticast = pCurrAddresses->FirstMulticastAddress;
			if (pMulticast) {
				for (i = 0; pMulticast != NULL; i++)
					pMulticast = pMulticast->Next;
				texto.Append("	\nNumber of Multicast Addresses : ");
				texto.AppendNumber(i);
			}
			//else
			//	printf("\tNo Multicast Addresses\n");

			pDnServer = pCurrAddresses->FirstDnsServerAddress;
			if (pDnServer) {
				for (i = 0; pDnServer != NULL; i++)
					pDnServer = pDnServer->Next;
				texto.Append("	\nNumber of DNS Server Addresses : ");
				texto.AppendNumber(i);
			}
			//else
			//	CAlert::InformationAlert("\tNo DNS Server Addresses\n");

			texto.Append("	\nDNS Suffix: "+PMString(WideString(pCurrAddresses->DnsSuffix)));// , pCurrAddresses->DnsSuffix);
			texto.Append("	\nDescription:" + PMString(WideString(pCurrAddresses->Description)));//, pCurrAddresses->Description);
			PMString FriendlyName = PMString(WideString(pCurrAddresses->FriendlyName));
			texto.Append("	\nFriendly name: "+ FriendlyName);//, pCurrAddresses->FriendlyName);

			PMString physical_Address = "";
			if (pCurrAddresses->PhysicalAddressLength != 0) {
				texto.Append("	\nPhysical address: ");
				for (i = 0; i < (int)pCurrAddresses->PhysicalAddressLength;
					i++) {
					if (i == (pCurrAddresses->PhysicalAddressLength - 1)) {
						physical_Address.AppendNumber((int)pCurrAddresses->PhysicalAddress[i]);//printf("%.2X\n",(int)pCurrAddresses->PhysicalAddress[i]);
					}
					else {
						physical_Address.AppendNumber((int)pCurrAddresses->PhysicalAddress[i]);
						physical_Address.Append("-");
					}
				}
			}

			IPAddress = physical_Address;
			if (FriendlyName == "Ethernet0")
				salir = kTrue;
			texto.Append(physical_Address+"\n");
			texto.Append("\nFlags: ");
			texto.AppendNumber((int)pCurrAddresses->Flags);
			
			texto.Append("\n Mtu: ");
			texto.AppendNumber((int)pCurrAddresses->Mtu);

			texto.Append("\n IfType: ");
			texto.AppendNumber((int)pCurrAddresses->IfType);

			texto.Append("\n OperStatus: ");
			texto.AppendNumber((int)pCurrAddresses->OperStatus);

			texto.Append("\n Ipv6IfIndex: ");
			texto.AppendNumber((int)pCurrAddresses->Ipv6IfIndex);

			texto.Append("\n tZoneIndices (hex): ");
		
			for (i = 0; i < 16; i++) {
				texto.AppendNumber(pCurrAddresses->ZoneIndices[i]);
				texto.Append(" ");
			}
				//CAlert::InformationAlert("%lx ");// , pCurrAddresses->ZoneIndices[i]);
			//printf("\n");

			texto.Append("\n Transmit link speed:");
			texto.AppendNumber(pCurrAddresses->TransmitLinkSpeed);

			texto.Append("\n Receive link speed:");
			texto.AppendNumber(pCurrAddresses->ReceiveLinkSpeed);

			pPrefix = pCurrAddresses->FirstPrefix;
			if (pPrefix) {
				for (i = 0; pPrefix != NULL; i++)
					pPrefix = pPrefix->Next;
				texto.Append("	\nNumber of IP Adapter Prefix entries");
				texto.AppendNumber(i);
				
				//CAlert::InformationAlert("\tNumber of IP Adapter Prefix entries: %d\n");// , i);
			}
			//else
			//	CAlert::InformationAlert("\tNumber of IP Adapter Prefix entries: 0\n");

			//CAlert::InformationAlert(texto);
			//printf("\n");

			pCurrAddresses = pCurrAddresses->Next;
			
		}
	}
	/*else {
		CAlert::ErrorAlert("Call to GetAdaptersAddresses failed with error");
		printf("Call to GetAdaptersAddresses failed with error: %d\n",
			dwRetVal);
		if (dwRetVal == ERROR_NO_DATA)
			printf("\tNo addresses were found for the requested parameters\n");
		else {

			if (FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER |
				FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
				NULL, dwRetVal, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
				// Default language
				(LPTSTR)& lpMsgBuf, 0, NULL)) {
				printf("\tError: %s", lpMsgBuf);
				LocalFree(lpMsgBuf);
				if (pAddresses)
					FREE(pAddresses);
				exit(1);
			}
		}
		
	}*/
	if (pAddresses) {
		FREE(pAddresses);
	}
#endif
    return IPAddress;
}

PMString InterlasaUtilities::getTmpDir()
{
    IDFile appPath;
    FileUtils::GetAppPath(&appPath);
    PMString volumeName="";
    FileUtils::GetVolumeName(&appPath, &volumeName);
    SDKUtilities::AppendPathSeparator(volumeName);
    volumeName.Append("private");
    SDKUtilities::AppendPathSeparator(volumeName);
    volumeName.Append("tmp");
    
    return volumeName;
}



/*
 */
static char gBuffer[2048];
#ifdef WINDOWS
#define vsnprintf _vsnprintf
namespace std {
    using ::_vsnprintf;
}
#endif

/*
 */
PMString InterlasaUtilities::SNIPLOG(const char* str, ...)
{
    va_list args;
    va_start(args, str);
    std::vsnprintf(gBuffer, sizeof(gBuffer) - 1, str, args);
    va_end(args);
    
    PMString pmstr;
    pmstr.SetCString(gBuffer, PMString::kUnknownEncoding);
    //SnipRunLog::Instance()->Log(pmstr);
    
    return pmstr;
}



int InterlasaUtilities::HexadecimalToDecimal(PMString  hex) {
    
    int hexLength = hex.NumUTF16TextChars();
    double dec = 0;
    int CX=0;
    for (int i = 0; i < hexLength; ++i)
    {
        UTF32TextChar b = hex.GetWChar(i);
       
        if (b.GetValue() >= 48 && b.GetValue() <= 57)
            CX = b.GetValue() - 48;
        else if (b.GetValue() >= 65 && b.GetValue() <= 70)
            CX = b.GetValue() - 55;
        //15 + 7
        //15 + 112
        dec += CX * pow(16, ((hexLength - i) - 1));
    }
    
    return (int)dec;
}


RGBInterlasa InterlasaUtilities::HexadecimalToRGB(PMString hexPM) {
    
    hexPM.ToUpper();

    int32 r = HexadecimalToDecimal(   *hexPM.Substring(1, 2)    );
    int32 g = HexadecimalToDecimal(   *hexPM.Substring(3, 2)    );
    int32 b = HexadecimalToDecimal(   *hexPM.Substring(5, 2)    );
    
    return RGBInterlasa(r, g, b);
}


